-- +goose Up
-- SQL in this section is executed when the migration is applied.

create table users
(
    id         serial primary key,
    email      text      not null,
    password   text      not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now(),
    deleted_at timestamp
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

drop table users;
