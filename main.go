package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/go-redis/redis/v7"
	"google.golang.org/grpc"

	pb "gitlab.com/trcharts/trcharts"

	"gitlab.com/trcharts/back/internal"
	"gitlab.com/trcharts/back/internal/config"
	"gitlab.com/trcharts/back/internal/connect"
)

func main() {
	confPath := flag.String("c", "/config.yaml", "path to config")
	flag.Parse()

	conf, err := config.LoadConfig(*confPath)
	if err != nil {
		log.Fatal(err)
	}

	conn, _ := grpc.Dial(fmt.Sprintf("%s:%d", conf.Trchartsbin.Host, conf.Trchartsbin.Port), grpc.WithInsecure())
	defer conn.Close()

	client := pb.NewTrchartsbinClient(conn)

	rd, err := redisClient(conf.Redis)
	if err != nil {
		log.Fatal(err)
	}

	db, err := connect.DBConnect(conf.Postgres)
	if err != nil {
		log.Fatal(err)
	}

	h, err := internal.NewHandlers(rd, db, client)
	if err != nil {
		log.Fatal(err)
	}

	routes := h.Routes()

	fmt.Println("server is up")
	err = http.ListenAndServe(":3000", routes)
	if err != nil {
		log.Fatal(err)
	}
}

func redisClient(conf config.RedisConfig) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", conf.Host, conf.Port),
		Password: conf.Password, // no password set
		DB:       0,             // use default DB
	})

	_, err := client.Ping().Result()
	return client, err
}
