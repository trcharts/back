package internal

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-redis/redis/v7"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/gorilla/websocket"
	"github.com/jmoiron/sqlx"
	pb "gitlab.com/trcharts/trcharts"

	"gitlab.com/trcharts/back/internal/auth"
)

type handlers struct {
	rd        *redis.Client
	db        *sqlx.DB
	binClient pb.TrchartsbinClient
}

func NewHandlers(rd *redis.Client, db *sqlx.DB, bin pb.TrchartsbinClient) (*handlers, error) {
	switch {
	case rd == nil:
		return nil, errors.New("redis is nil")
	case db == nil:
		return nil, errors.New("db is nil")
	case bin == nil:
		return nil, errors.New("bin is nil")
	}

	return &handlers{
		rd:        rd,
		db:        db,
		binClient: bin,
	}, nil
}

func (h *handlers) Routes() *chi.Mux {
	corsMiddleware := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}).Handler
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(corsMiddleware)

	r.Route("/auth", auth.Routes)

	r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		_ = json.NewEncoder(w).Encode("pong")
	})
	r.Get("/ws/ticker/{src}/interval/{interval}/symbol/{symbol}", h.tickerSubscribe)
	r.Get("/klines", h.getKlines)

	return r
}

func (h *handlers) tickerSubscribe(w http.ResponseWriter, r *http.Request) {
	source := chi.URLParam(r, "src")
	interval := chi.URLParam(r, "interval")
	symbol := chi.URLParam(r, "symbol")

	if source != "BIN" || interval != "1m" {
		_ = json.NewEncoder(w).Encode(errors.New("wrong source or interval"))

		w.WriteHeader(http.StatusBadRequest)
		return
	}

	ws := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	conn, err := ws.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
		_ = json.NewEncoder(w).Encode(err) // TODO: move to logs and delete http response (security)

		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, err = h.binClient.SubIndividualTicker(context.Background(), &pb.Text{Value: symbol})
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	errChan := make(chan error)
	pub := h.rd.Subscribe(fmt.Sprintf("BIN:TICKER:1m:%s", symbol))
	defer pub.Unsubscribe(fmt.Sprintf("BIN:TICKER:1m:%s", symbol))

	go func() {
		_, _, inErr := conn.ReadMessage()
		if inErr != nil {
			if websocket.IsCloseError(inErr, 1005) {
				errChan <- nil
			}
			errChan <- inErr
		}
	}()

	go func(pubsub *redis.PubSub) {
		for {
			msg, err := pubsub.ReceiveMessage()
			if err != nil {
				if err == io.EOF {
					errChan <- nil
					return
				}

				errChan <- err
				return
			}

			t := pb.Ticker{}
			err = proto.Unmarshal([]byte(msg.Payload), &t)
			if err != nil {
				errChan <- nil
			}

			err = conn.WriteJSON(t.String())
			if err != nil {
				if err == io.EOF {
					errChan <- nil
					return
				}

				errChan <- err
				return
			}
		}
	}(pub)

	errCh := <-errChan
	if errCh != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	defer conn.Close()
}

func (h *handlers) getKlines(w http.ResponseWriter, r *http.Request) {
	symbol := r.URL.Query().Get("symbol")
	interval := r.URL.Query().Get("interval")
	start := r.URL.Query().Get("start_time")
	end := r.URL.Query().Get("end_time")

	startTime, err := time.Parse(time.RFC3339, start)
	if err != nil {
		_ = json.NewEncoder(w).Encode(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	endTime, err := time.Parse(time.RFC3339, end)
	if err != nil {
		_ = json.NewEncoder(w).Encode(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	startProt, _ := ptypes.TimestampProto(startTime)
	endProt, _ := ptypes.TimestampProto(endTime)

	res, err := h.binClient.GetKLines(r.Context(), &pb.KLinesReq{Symbol: symbol, Interval: interval, StartTime: startProt, EndTime: endProt})
	if err != nil {
		_ = json.NewEncoder(w).Encode(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	type KLine struct {
		Symbol      string    `json:"symbol,omitempty"`
		Open        string    `json:"open,omitempty"`
		High        string    `json:"high,omitempty"`
		Low         string    `json:"low,omitempty"`
		Close       string    `json:"close,omitempty"`
		Volume      string    `json:"volume,omitempty"`
		QuoteVolume string    `json:"quote_volume,omitempty"`
		OpenTime    time.Time `json:"open_time,omitempty"`
		CloseTime   time.Time `json:"close_time,omitempty"`
		TradesCount int32     `json:"trades_count,omitempty"`
	}

	response := make([]KLine, len(res.Klines))

	for i := range res.Klines {
		openTime, _ := ptypes.Timestamp(res.Klines[i].OpenTime)
		closeTime, _ := ptypes.Timestamp(res.Klines[i].CloseTime)

		response[i] = KLine{
			Symbol:      res.Klines[i].Symbol,
			Open:        res.Klines[i].Open,
			High:        res.Klines[i].High,
			Low:         res.Klines[i].Low,
			Close:       res.Klines[i].Close,
			Volume:      res.Klines[i].Volume,
			QuoteVolume: res.Klines[i].QuoteVolume,
			TradesCount: res.Klines[i].TradesCount,
			OpenTime:    openTime,
			CloseTime:   closeTime,
		}
	}

	_ = json.NewEncoder(w).Encode(response)
}
