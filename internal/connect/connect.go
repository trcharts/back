package connect

import (
	"fmt"

	"github.com/go-redis/redis/v7"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"

	"gitlab.com/trcharts/back/internal/config"
)

func DBConnect(conf config.PostgresConfig) (*sqlx.DB, error) {
	str := fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s",
		conf.Host,
		conf.Port,
		conf.DB,
		conf.User,
		conf.Password,
		conf.SSLMode,
	)
	db, err := sqlx.Connect("pgx", str)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func RedisConnect(conf config.RedisConfig) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", conf.Host, conf.Port),
		Password: conf.Password,
		DB:       0,
	})

	_, err := client.Ping().Result()
	return client, err
}
