package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v3"
)

type HostPortConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

type PostgresConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	DB       string `yaml:"db"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	SSLMode  string `yaml:"sslmode"`
}

type RedisConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Password string `yaml:"password"`
}

type DownloaderConfig HostPortConfig

type TrchartsbinConfig HostPortConfig

type Config struct {
	Postgres    PostgresConfig    `yaml:"postgres"`
	Redis       RedisConfig       `yaml:"redis"`
	Downloader  DownloaderConfig  `yaml:"bin_downloader"`
	Trchartsbin TrchartsbinConfig `yaml:"trchartsbin"`
}

func LoadConfig(path string) (Config, error) {
	conf := Config{}

	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		return Config{}, err
	}
	err = yaml.Unmarshal(yamlFile, &conf)
	if err != nil {
		return Config{}, err
	}

	return conf, nil
}
