module gitlab.com/trcharts/back

go 1.14

require (
	github.com/Masterminds/squirrel v1.4.0
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/cors v1.0.1
	github.com/go-redis/redis/v7 v7.2.0
	github.com/golang/protobuf v1.3.5
	github.com/gorilla/websocket v1.4.2
	github.com/jackc/pgx/v4 v4.6.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.2.0
	gitlab.com/trcharts/trcharts v0.0.3
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	google.golang.org/grpc v1.28.0
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
